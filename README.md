# tonnetz
Hex grid that plays the notes in a [Tonnetz Triangle](https://en.wikipedia.org/wiki/Tonnetz) arrangement.
To play a note, hover on a hexagon.

## Install
`npm install --save d3`
`npm install --save d3-hexbin`

Code taken from [Tonal](https://github.com/danigb/tonal) and [Tone.js](https://github.com/Tonejs/Tone.js/).
